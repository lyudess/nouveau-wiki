## The irregular Nouveau-Development companion


## Issue for January, 9th


### Intro

First of all, a Happy New Year to all of you. Thanks for your continued interest in our project. 

I came back after a much needed break, just to notice that I missed a quite a bunch of things. So it is time again for a new issue of the TiNDC. Without further delay, here we go: 


### Current status

Phoronix.com did an overview article on our project, which was quite favorable and correct. However, linking to our not up to date feature matrix instead to the TiNDC was unfortunate. But see for yourself here: [[http://www.phoronix.com/vr.php?view=8293|http://www.phoronix.com/vr.php?view=8293]] That article made it rounds on digg.com, linux-gamers.net and osnews.com so that we got quite some more dumps for different / missing card types. 

The pledge mentioned is however not supported by our project. We currently don't need any money and the person who set it up is not connected to our project. See [[http://lovesunix.net/blog/?p=153|http://lovesunix.net/blog/?p=153]] (Link from our [[InThePress|InThePress]] page) for some more background. 

As it was Christmas, pq sacrificed some of his time to enhance dual card setup in renouveau, especially device detection. All looked good until  darktama spotted a minor error in the AGP aperture detection. That was  promptly fixed by pq. Tests on a SLI setup with renouveau however still resulted in problems, so something is still missing here. 

jrmuizel offered his help by catching accesses to the MMIO registers of the card. Basically he wrote a kernel module which is triggered by such an access and logs the values of the registries before and after the access. Now, why is this needed? Currently renouveau only dumps the state of the complete register memory region. This probably contains much noise in that if one register is written, the content of others may change. So knowing which values are written to which registers would really help finishing the work on initialisation (Remember: At this point Darktama's code still relied on the init done by the binary blob and only after that is done, takes over the card - but read on). As it currently stands, the code works mostly fine but was only tested on pq's  card. Dumps were made available by pq but Darktama needed more data points, as airlied's try on PPC resulted in invisible gears. 

So, seeing by pq's example that it was possible to survive, KoalaBR "valiantly" offered help, too. Setting up MMIO, another dump was created and after some  studying Darktama was confident to have found the correct init values and registers. One day later his latest patch was ready and once again KoalaBR offered help. 

Setting up Mesa libs, current DRM and 2D driver, glxgears was started  and crashed with a segmentation violation error. Trying to run glxgears  under gdb (and subsequently again on its own) resulted in flawless  acceleration. So it seems that on NV4x cards we have initialisation and some very basic 3D acceleration working. But still we seem to have 

* at least one bug on PPC which prevents correct rendering 
* one basic bug during init of the 3D part of the driver, which may result in SEGV errors. 
* messed up PPC, as airlied reported back more problems on PPC than before. 
Meanwhile pq and jrmuizel worked on a newer version of mmio. Goal was to make  it more stable and refrain from adding more hooks to the kernel in order to  get it to work. As of 8.1.2007 (European date format), this work is still ongoing because current versions sooner or later enter a endless loop rendering the system useless until reboot. 

pq and pmdata continued their quest to find out the missing spotlight parameters. Though some progress was made the topic still isn't resolved. 

airlied added a new branch to the git repository which enables randr-1.2 for nouveau. This includes the start of work for the new mode setting code. RandR-1.2 for nouveau does need some testers. Please contact airlied if you can offer help. As of now, airlied got dual head analogue output on his NV28 working. 

Our call for help on crashes on 7x00 cards was heard by hughsie who offered his help. Marcheu tried to figure it out, but wasn't able to find the reason for the crashes. It seems like certain types of NVidia GPUs have a hardware bug which can't handle overlapping memory copies, resulting in FIFO hangs. We need further testing to find out how to work around it (software fallback is one likely solution). 

Not to be left behind darktama's success on NV4x cards, mat<ins> continued his analysis of the context switches on cards up to NV2x. Seems that these cards do switching exclusively through the driver. This includes some register  manipulation on NV10 cards and PGRAPH_PIPE saving and restoring. </ins> 

<ins>Our wiki here is no longer "english only": Maximi89 started translating some important pages to spanish. That is a huge undertaking as quite a few pages are changing during the week. Thanks you for your efforts Maximi89! </ins> 

<ins>Fedora Core 7 will ship with our drivers added (hopefully not as default, as  we are still far away from being useful regarding 3D acceleration). See  announcement here: [[http://www.redhat.com/archives/fedora-devel-list/2007-January/msg00091.html|http://www.redhat.com/archives/fedora-devel-list/2007-January/msg00091.html]] Says marcheu: "cool, I suppose we'll need a bunker of some kind to hide from  angry fedora users though" </ins> 


### Help needed

As already mentioned above: if you are a developer using some kind of dual head setup, please contact airlied. If you have a SLI setup, please give renouveau a try and report back to pq. Furthermore, if you are on PowerPC you could try to run nouveau with 3D enabled and report back to Darktama or Airlied. And finally, if you can code and want to help out, please have a look at our [[ToDo|ToDo]] page, especially the [Junior] jobs. 

And that's it for this issue. We hope, you enjoyed reading TiNDC. 

[[<<< Previous Issue|NouveauCompanion_10]] | [[Next Issue >>>|NouveauCompanion_12]] 
