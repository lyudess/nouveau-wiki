Quoting [[HonzaHavlicek|HonzaHavlicek]]: 

* The nv_demos are small programs used for RE of nvidia commands and registers. They are not drivers and they do not use DRI/Gallium3D. They manually set up objects and write some commands to FIFO. The developer write some code to nv_demo and hopes to achieve some result (triangle, repated texture, scissors..). She will achieve the expected result (mostly) after a few (painfull) hours (days). General principle is to look at the MMIO and renouveau dumps and try to repeat what nvidia blob did. Sometimes it is a single bit, sometimes the whole infrastructure. 
      * nv04_demo [[http://nouveau.cvs.sourceforge.net/nouveau/nv04_demo/|http://nouveau.cvs.sourceforge.net/nouveau/nv04_demo/]] 
      * nv10_demo [[http://nouveau.cvs.sourceforge.net/nouveau/nv10_demo/|http://nouveau.cvs.sourceforge.net/nouveau/nv10_demo/]] 
      * nv20_demo [[http://cgit.freedesktop.org/~pq/nv20_demo/|http://cgit.freedesktop.org/~pq/nv20_demo/]] 
      * nv30_demo [[http://cgit.freedesktop.org/~jkolb/nv30_demo/|http://cgit.freedesktop.org/~jkolb/nv30_demo/]] 
      * another nv30_demo [[http://cgit.freedesktop.org/~pmandin/nv30_demo/|http://cgit.freedesktop.org/~pmandin/nv30_demo/]] 
      * nv40_demo [[http://nouveau.cvs.sourceforge.net/nouveau/nv40_demo/|http://nouveau.cvs.sourceforge.net/nouveau/nv40_demo/]] 

## Instructions (nv10_demo in this example)

   * [[!format txt """
  cvs -d:pserver:anonymous@nouveau.cvs.sourceforge.net:/cvsroot/nouveau login
  cvs -z3 -d:pserver:anonymous@nouveau.cvs.sourceforge.net:/cvsroot/nouveau co -P nv10_demo
"""]]
Then enter `nv10_demo` and just run `make`. 

The binary requires access to the drm module, run it using root: `sudo ./nouveau_demo`. 
