[[!table header="no" class="mointable" data="""
 [[Home|FrontPage]]  |  [[TiNDC 2006|IrcChatLogs]]  |  [[TiNDC 2007|IrcChatLogs]]  |  [[Old Logs|IrcChatLogs]]  |  [[Current Logs|IrcChatLogs]]  | DE/[[EN|Nouveau_Companion_27]]/[[ES|Nouveau_Companion_27-es]]/[[FR|Nouveau_Companion_27-fr]]/RU/[[Team|Translation_Team]] 
"""]]


## Informe Irregular sobre el Desarrollo de Nouveau


## Edición del 3 de octubre


### Introducción

Bien. De vuelta de vacaciones e intentando resumir cuatro semanas de progreso. 

Pero, antes de empezar, algunos comentarios recibidos acerca de nuestro traslado a Phoronix. En resumen: generamos la sospecha de vendernos. 

Me sientro realmente halagado. Jamás me habría imaginado que tenemos lectores tan apasionados por el TiNDC y dónde se publica antes. 

Entonces, ¿por qué nos trasladamos?. Fundamentalmente para tener la posibilidad de dirigirnos a una audiencia más amplia y, tal vez, atraer a nuevos colaboradores entre ella. Estamos siendo pragmáticos: Todo lo que pueda ayudar al proyecto a avanzar merece la pena ser probado, y lo hacemos no tanto por nosotros mismos sino por vosotros. El no intentarlo nos parece una pérdida o una rendición. 

Antes de juzgar la utilidad de nuestras acciones... ¿porqué no dar una oportunidad? En los primeros 3 días tuvimos 14.000 hits en phoronix.com y estos se produjeron incluso antes de que el artículo fuese mencionado en Linuxgames.com y digg. Si esto resulta en más colaboradores o no está todavía por ver. 

Los próximos números no tendrán un retraso tan grande entre la publicación en phoronix y la del wiki, aunque, debido a cuestiones de zonas horarias, habrá un desfase de aproximadamente 1 día. 

Bien. Desde que salió nuestro último TiNDC se ha producido la publicación de especificaciones por parte de AMD a la comunidad del código abierto. Estamos encantados de que nuestros colegas de radeon tengan tanta suerte. ¡Ojalá Nvidia vea también la luz algún día! 


### Estado actual

A fines de agosto también se produjo el traslado de algunos de nuestros servicios desde cuentas personales (en el instituto, universidad, etc) a fd.o. La página de estado de JussiP, el script de actualización de los directorios de volcados desde gmail de Kmeyer, y el registro de Marcheu, se trasladaron a fd.o. 

Con el trabajo realizado hasta el momento por ahuillet en Xv, la atención de los desarrolladores se trasladó a otras funcionalidades. 

En primer lugar, stillunknown, que portó nuestro código de detección de dispositivo a libpciaccess. Esta biblioteca es la nueva forma de Xorg 7.3 (xserver 1.4) para manejar los accesos al hardware de forma independiente de la plataforma, solucionando algunas de las quejas más importantes sobre la detección del hardware en Xorg. Las pruebas posteriores de Marcheu en una NV04 mostraron que funcionaba. 

Durante la discusión inicial de planes entre marcheu y Thunderbird, marcheu advirtió que quería deshacerse de la lista de PCI-IDs en nouveau. Quería utilizar solamente registros (como PMC_BOOT_0) para la detección de arquitectura y evitar cualquier cosa que se refiera a una tarjeta específica y no una arquitectura. Stillunknown actuó en consecuencia y presentó un parche que funcionaba con PMC_BOOT_0 (en NV50 se usa el registro con desplazamiento $88000 en su lugar). 

Mientras, pmdata y matc libraron una dura batalla con la Denegación de Servicio que es glxgears en tarjetas NV1x. Durante los últimos número se solucionaron algunos problemas (como el tamaño incorrecto de FIFO, manejo incorrecto de las interrupciones PGRAPH) que poco a poco, pero sin pausa, hicieron "funcionar" mejor glxgears. Sin embargo, todavía no se obtiene salida alguna. 

Probando algunas de las lecciones de OpenGL de NEHE, pmdata advirtió que las matrices de vista de modelo y de proyección no se estaban manejando correctamente. p0g probó suerte mostrando un único triángulo. Por tanto, usó nv10_demo, comprobó un volcado de renouveau de su tarjeta, e incluyó todos los datos de inicialización del volcado en el programa. Esto produjo algunas interrupciones de estado inválido, por lo que todo lo que producía el estado inválido fue eliminado nuevamente. 

Al final, se mostró un triángulo, pero solamente en circunstancias muy especiales (como algunas posiciones de la ventana) y el color todavía era erróneo. p0g investigó algo más y encontró que los valores VIEWPORT_SCALE_[XYZ] no se calculaban correctamente. Integró sus cambios a la demostración en la que trabajaba pmdata, probando suerte. Tras algunas correcciones menores (la resolución estaba fijada a 1280x1024), finalmente localizó el error de su código: la máscara de color se fijaba mediante un valor en coma flotante, mientras que debe ser un valor entero. De nuevo fue p0g el que encontró otro error: el orden en que se enviaban los datos de color y posición a la tarjeta era incorrecta. 

Así que, tras mucho trabajo, pmdata obtuvo finalmente lo siguiente: 

[[!img 20070906.png] 

En este punto, lo único que faltaba era una inicialización adecuada de buffer de profundidad. Un pequeño hack de Mesa que hizo p0g resultó en lo siguiente: 

[[!img nv18-glxgears.png] 

y llevó a pmdata al último fallo: la inicialización del buffer de estado del buffer de profundidad era incorrecta, y, una vez que se arregló esto, pmdata vió glxgears en marcha. Nos gustaría enviar también agradecimientos a p0g, ya que realizó un trabajo muy importante de apoyo, pero de alguna manera no he podido encontrar un tema para poder mencionarlo dentro de contexto. Lo siento :) 

Per, ya que todavía tenemos algunos problemas con el recortado, la modificación del tamaño de la ventana producirá algunos resultados extraños. 

Tras trabajar un poco en EXA en NV3x, marcheu cambió a los problemas de drm en NV04. El trabajo en nv30 consistía en evitar el tener que ejecutar el driver binario antes de nouveau para ser capaz de usar el subsistema de 3d. 

La razón del cambio a NV04 fue que ahuillet estaba a la espera de una donación de una tarjeta NV04 de un colaborador de nuestro proyecto (¡Gracias!). Pero, ya que el DRM estaba roto por la actualización de "TTM" (reservar una FIFO para el DRM), era necesario lograr que funcionase el cambio de contexto. Algunos cambios ya los había hecho marcheu con anterioridad, pero los perdió tras sus problemas de disco duro que mencionamos en el número anterior. Por ello se propuso volver a hacer el trabajo. 

Así, nv04_demo es ahora capaz de dibujar triángulos en NV04  :) 

Al intentar hacer funcionar NV04, Marcheu observó algunos problemas extraños y difíciles de reproducir en Xv, lo que motivó a ahuillet para ponerse manos a la masa con la tarjeta NV04 que le envió unbeliever (¡Muchas gracias!). El primer análisis resultó en la sospecha de que el código de blitter en NV04 no estaba funcionando correctamente, probablemente debido a las limitaciones de tamaño (de video) en la ruta de código de DMA. 

Cuando se forzaba la ruta de copia por CPU, entonces todo funcionaba bien, es decir, parpadeaba, pero eso se debía a otro problema. Por lo que existena algunos fallos en la implementación de Xv en NV04 pero solamente se pueden provocar utilizando una resolución realmente alta y, por tanto, se consideran menores. 

Como última nota sobre Xv por hoy: ahuilleta también implementó la superposición para NV04. 

En el frente de las G80 (¡no las G84!), Darktama y maccam94 hicieron algunas pruebas y vieron que el 2D funcionaba, aunque con algunos problemillas (como la omisión por cortos periodos de tiempo de partes de las ventanas al refrescar o mover las ventanas). 

Airlied volvió a integrar la rama Randr 1.2 en la rama principal de desarrollo, lo que significa que todas las funcionalidades disponibles o en desarrollo están ahora disponibles a través de la rama principal. Para habilitar Randr1.2 con nouveau en la rama pincipal, por favor, usad: Option "Randr12" "TRUE". Las pruebas mostraron que el blitter de Xv estaba roto para la configuración de doble salida (dual head). 

Bk y stillunknown continuaron el trabajo sobre randr12 y lo dejaron en mejores condiciones. Es decir, debería funcionar en más tarjetas que antes. 

Las versiones iniciales de xserver producían algunos errores de compilación debido a la reciente adición de inclusiones, etc, pero pmdata se adjudicó la tarea de arreglarlos uno a uno. Por lo que ahora nouveau debería también funcionar en xserver 1.1, aunque, obviamente, sin ningún soporte de randr1.2. pmdata añadió una opción de configuración "--disable-nv30exa" que permite usar xserver >= 1.1 para ejecutar y compilar nouveau. 

Stillunknown detectó algunos problemas (p.e. la ausencia de algunos prototipos de funciones) y se los comunicó a Darktama. Asumió la tarea de probarlo en su hardware y eliminó algunos de los avisos durante el proceso. 

Llegaron muchos más informes sobre el soporte de randr1.2, la mayoría indicando que fundamentalmente funcionaba. Lo más molesto es que, en los CRTs analógicos la salida parece desplazarse a la derecha y muestra un borde púrpura o rosa en el lateral derecho. Stillunknown todavía está investigando en la oscuridad, ya que sólamente dispone de una pantalla LCD (DFP) a su disposición. 

Por ello habló con Thunderbird y airlied sobre dichos problemas y ambos le ayudaron ofreciendo la informacion de la que disponían. 

bk trabajó algo en suspensión y recuperación (desde/a disco), tanteando las necesidades de nouveau. La situación se complica al faltar actualmente en el kernel la infraestructura necesaria para guardar y modificar los modos de video. 

Tras dos días logró hacerlo funcionar, sin bloqueos, y logrando que X encontrase la mayor parte de los datos del contexto de la tarjeta. Sin embargo, X todavía se colgaba tras la recuperación, con un cuelgue de DMA. 

Algunos temas breves: 

* - pq es ahora el mantenedor oficial de MMioTrace. Ya ha solucionado algunos errores de 
      * escritura de registros y planea algunos parámetros adicionales para el módulo. - Se ha eliminado finalmente el soporte para XAA. - Fedora 8 contiene ahora una versión actual de nouveau (a 20.09.2007), gracias a 
      * airlied. - También, gracias a airlied, la mayor parte de los cambios del TTM solicitados por nuestro 
      * proyecto están hechos. - Marcheu y Darktama estuvieron en la Conferencia de Desarrolladores de X.org - En PPC los cambios de contexto con una NV17 no funcionan. Marcheu expresó interés en arreglarlo. 
      * Pensaba que el problema se debe al traslado de los notificadores de VRAM al espacio de PCI. Un parche rápido y una prueba más tarde, y PPC ya estaba de nuevo en funcionamiento. 
Un último tema: Estamos realmente intentando dar soporte a todas las tarjetas NVidia, desde la Riva TNT (NV04) en adelante, pero durante las últimas semanas nos hechos dado cuenta de que no podemos implementar EXA en una NV04. Básicamente se debe al hecho de que las NV04 no son capaces de manejar texturas rectangulares, únicamente teseladas, lo que no encaja bien con EXA. 

Para acelerar EXA usando el subsistema de 3D, necesitaríamos combinar/mezclar teselas, y esto no es posible, ya que EXA nos proporciona una serie de superficies que no están teseladas. El subsistema 3D de NV04 espera, sin embargo, superficies teseladas como origen. Tendríamos que teselar las superficies de origen al vuelo, lo que es bastante complicado. A todos los usuarios de NV04 que nos lean: realmente lo sentimos. 

Por cierto... las "auténticas" NV04 (Nvidia TNT 1) no funcionan todavía con nouveau ya que carecen de algunos métodos que es necesario implementar por software. Darktama quiere implementar métodos por software para NV50, por lo que ahuillet está esperando que los incluya en el reservorio de nouveau, para añadir el soporte de NV04. 


### Ayuda requerida

Por favor, probad los modos randr 1.2 e informad a stillunknown o bk. 

Los problemas con PPC deben reportarse a marcheu. 


### Gracias

En si mismo, el TiNDC no es solamente un resumen del trabajo hecho, sino también un agradecimiento al dedicado esfuerzo de nuestros desarrolladores. Pero en esta ocasión tenemos algunas donaciones de hardware por las que querríamos dar también las gracias: 

Un breve gracias en esta ocasión a evanfraser por proporcionarnos un par de auténticas tarjetas quadro (NV3x). Una de ellas está ya en manos de Darktama y la otra fue "adoptada" por nuestro valiente líder de proyecto. 

Y gracias por proporcionar a ahuillet tarjetas NV04, NV05, NV11 y NV15. Gracias unbeliever y andreasn. 

Todos los desarrolladores están ya trabajando como locos para intentar dar soporte a esas tarjetas :) 
