# Nouveau: Accelerated Open Source driver for nVidia cards

The **nouveau** project aims to build high-quality, free/libre software drivers for [[nVidia cards|CodeNames]].  “Nouveau” [*nuvo*] is the French word for “new”. Nouveau is composed of a Linux kernel KMS driver (nouveau), Gallium3D drivers in Mesa, and the Xorg DDX (xf86-video-nouveau). The kernel components have also been ported to [[NetBSD]].

## Current Status

- 2D/3D acceleration supported on all GPUs; see [[FeatureMatrix]] for details.
- Automatic reclocking available for Turing and newer through GSP firmware.
- Video decoding acceleration supported on most pre-Maxwell cards; see [[VideoAcceleration]] for details.
- Support for manual performance level selection (also known as "reclocking") on [[GM10x Maxwell|CodeNames#nv110familymaxwell]], [[Kepler|CodeNames#nve0familykepler]] and [[Tesla G94-GT218|CodeNames#nv50familytesla]] GPUs. Available in `/sys/kernel/debug/dri/0/pstate`
- Little hope of reclocking becoming available for [[GM20x|CodeNames#nv110familymaxwell]], [[GP10x|CodeNames#nv130familypascal]] and [[GV100|CodeNames#nv140familyvolta]] as firmware now needs to be signed by NVIDIA to have the necessary access.

## News

- Nov, 2023: [[AD10x|CodeNames#nv190familyadalovelace]] and GSP support merged in Linux 6.7.
- Jan, 2021: [[GA10x|CodeNames#nv170familyampere]] kernel mode setting support merged in Linux 5.11.
- Jan, 2019: [[TU10x|CodeNames#nv160familyturing]] acceleration support (with redistributable signed firmware) merged in Linux 5.6.
- Jan, 2019: Support for Turing merged into Linux 5.0.
- Nov, 2018: Support for HDMI 2.0 high-speed clocks merged into Linux 4.20 (GM200+ hardware only).
- Aug, 2018: Support for Volta merged into Linux 4.19.

## Software

<table>
    <tr>
        <td>[[Linux Kernel|http://www.kernel.org/]]</td>
        <td>[[git|https://cgit.freedesktop.org/drm/drm-misc/]]</td>
    </tr>
    <tr>
        <td>libdrm</td>
        <td>[[git|http://cgit.freedesktop.org/mesa/drm/]]</td>
    </tr>
    <tr>
        <td>[[Mesa|http://www.mesa3d.org/]]</td>
        <td>[[git|http://cgit.freedesktop.org/mesa/mesa/]]</td>
    </tr>
    <tr>
        <td>xf86-video-nouveau</td>
        <td>[[git|http://cgit.freedesktop.org/nouveau/xf86-video-nouveau/]]</td>
    </tr>
</table>

## Contacting the Team

- IRC: #nouveau on [[OFTC|https://oftc.net]]. [[Logs|https://people.freedesktop.org/~cbrill/dri-log/index.php?channel=nouveau]], or [[IrcChatLogs]]
- Mailing lists: [[nouveau|https://lists.freedesktop.org/mailman/listinfo/nouveau]], [[dri-devel|https://lists.freedesktop.org/mailman/listinfo/dri-devel]], [[mesa-dev|https://lists.freedesktop.org/mailman/listinfo/mesa-dev]]

## Common Tasks

- [[Installing Nouveau|InstallNouveau]]
- Multiple monitors: [[Randr12]] and [[MultiMonitorDesktop]]
- Prime/Optimus: [[Optimus]]
- [[VDPAU]]/[[XvMC]]: [[Video decoding acceleration|VideoAcceleration]]
- [[KernelModuleParameters]]

## Get Involved

- Join us on IRC
- [[Become a developer|IntroductoryCourse]]
- [[Run tests on your hardware|TestersWanted]]
<!-- - Register as a tester -->
- [[Donate hardware|HardwareDonations]]
- [[Available tasks on Trello|https://trello.com/b/ZudRDiTL/nouveau]], [[CTS issues on Trello|https://trello.com/b/lfM6VGGA/nouveau-cts]], [[Old ToDo|ToDo]] list

## Report Bugs

- [[TroubleShooting]]
- [[Bugs]]
- [[FAQ]]

## Development/Debugging

- [[Development]] resources
- [[envytools|https://github.com/envytools/envytools/]]: RE tools and docs

*The content of this wiki is licensed under the [[MIT License|http://opensource.org/licenses/mit-license.php]] unless stated otherwise by the author of specific wiki pages.*